export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export HISTFILE="$ZDOTDIR/.zhistory"    # History filepath

export EDITOR="nvim"
export VISUAL="nvim"

export HISTSIZE=10000                   # Max for internal history
export SAVEHIST=10000

### Spaceship vars
export SPACESHIP_ROOT="$XDG_CONFIG_HOME/spaceship"
export SPACESHIP_TIME_SHOW=true
export SPACESHIP_TIME_12HR=true
export SPACESHIP_TIME_COLOR=magenta
export SPACESHIP_USER_COLOR=blue
export SPACESHIP_USER_SHOW=always
export SPACESHIP_PROMPT_ADD_NEWLINE=true
export SPACESHIP_CHAR_SYMBOL="❯"
export SPACESHIP_CHAR_SUFFIX=" "

export SPACESHIP_PROMPT_ORDER=(
  time		# Display time
  user          # Username section
  dir           # Current directory section
  host          # Hostname section
  git           # Git section (git_branch + git_status)
  hg            # Mercurial section (hg_branch  + hg_status)
  exec_time     # Execution time
  line_sep      # Line break
  jobs          # Background jobs indicator
  exit_code     # Exit code section
  char          # Prompt character
)
export SPACESHIP_RPROMPT_ORDER=(
  python        # python
  venv 	        # virtual env
	)

