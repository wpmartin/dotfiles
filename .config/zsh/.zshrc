# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

autoload -Uz compinit promptinit
compinit
promptinit

zstyle ':completion:*' menu select

setopt HIST_SAVE_NO_DUPS         # No duplicate events to the history file.

bindkey -v

path=(/opt/homebrew/bin $path)
path=(~/.local/bin $path)

source $ZDOTDIR/aliases
source "$HOME/.config/zsh-plugins/powerlevel10k/powerlevel10k.zsh-theme"
source "$HOME/.config/zsh-plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "$HOME/.config/zsh-plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
