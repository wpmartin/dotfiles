local wezterm = require("wezterm")
local mux = wezterm.mux
local act = wezterm.action

wezterm.on('gui-startup', function()
	local tab, pane, window = mux.spawn_window({})
end)

return {
	-- Smart tab bar [distraction-free mode]
	hide_tab_bar_if_only_one_tab = true,

	-- Color scheme
  color_scheme = "Catppuccin Mocha",

	window_background_opacity = 0.95,
	inactive_pane_hsb = {
		saturation = 0.8,
		brightness = 0.7
	},

	-- Font configuration
	-- https://wezfurlong.org/wezterm/config/fonts.html
	font = wezterm.font("Fira Code"),
	font_size = 12.0,

	-- Disable ligatures
	-- https://wezfurlong.org/wezterm/config/font-shaping.html
	harfbuzz_features = { "calt=0", "clig=0", "liga=0" },
	leader = { key = "a", mods = "CTRL", timeout_milliseconds = 3000 },
	keys = {
		-- Send "CTRL-A" to the terminal when pressing CTRL-A, CTRL-A
		{ key = "a", mods = "LEADER|CTRL", action = act({ SendString = "\x01" }) },
		{ key = "-", mods = "LEADER", action = act({ SplitVertical = { domain = "CurrentPaneDomain" } }) },
		{ key = "\\", mods = "LEADER", action = act({ SplitHorizontal = { domain = "CurrentPaneDomain" } }) },
    { key = 'h', mods = 'LEADER', action = act.ActivatePaneDirection 'Left', },
    { key = 'l', mods = 'LEADER', action = act.ActivatePaneDirection 'Right', },
    { key = 'k', mods = 'LEADER', action = act.ActivatePaneDirection 'Up', },
    { key = 'j', mods = 'LEADER', action = act.ActivatePaneDirection 'Down', },
	  { key = 'H', mods = 'LEADER', action = act.AdjustPaneSize { 'Left', 5 }, },
    { key = 'J', mods = 'LEADER', action = act.AdjustPaneSize { 'Down', 5 }, },
    { key = 'K', mods = 'LEADER', action = act.AdjustPaneSize { 'Up', 5 } },
    { key = 'L', mods = 'LEADER', action = act.AdjustPaneSize { 'Right', 5 }, },
    { key = 'm', mods = 'LEADER', action = wezterm.action.ToggleFullScreen, },
    { key = "z", mods = "LEADER", action = "TogglePaneZoomState" },
  },
}
-- vim: ts=2 sts=2 sw=2 et
