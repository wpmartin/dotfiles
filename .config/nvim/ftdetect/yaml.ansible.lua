vim.filetype.add{
    pattern = {
        ['.*/tasks/.*%.y[a]?ml'] = 'yaml.ansible',
        ['.*/tests/.*%.y[a]?ml'] = 'yaml.ansible',
        ['.*/handlers/.*%.y[a]?ml'] = 'yaml.ansible',
        ['.*/playbooks/.*%.y[a]?ml'] = 'yaml.ansible',
        ['.*/site.y[a]?ml'] = 'yaml.ansible',
        ['.*/main.y[a]?ml'] = 'yaml.ansible',
        ['.*/playbook.y[a]?ml'] = 'yaml.ansible',
    },
}
